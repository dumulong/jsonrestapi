// ---------------------------------------------------
// We've decided to put these values in a separate file as the user 
// may want to redefine them.
// It makes it easier to update the code without overwriting these values. 
// ---------------------------------------------------

const PAGESIZE = 200;
const PAGEWINDOW = 5;