
const API_URL = "/api/items";

const icons = {
  ICON_CLOSE : "\u{2212}",
  ICON_OPEN : "\u{002B}",
  ICON_SEARCH : "\u{1F50E}\u{FE0E}",
  ICON_EDIT : "\u{2710}",
  ICON_DELETE : "\u{1F5D1}",
  ICON_COLLAPSE_UP : "\u{25B2}",
  ICON_COLLAPSE_DOWN : "\u{25BC}",
  ICON_EXIT : "\u{2297}",
  ICON_COMMENT : "\u{22EF}"
}

const CONTINUE = " (CONTINUED...)";
const blankOption = '<option value="">Select or click check for new values</option>';

let timeoutId = 0;
let allItems =  [];
let filteredItems =  [];
let itemsHierarchy = {};
let usingAPI;
let pagination;

const searchParams = {
  deck : '',
  header : '',
  subheader : '',
  article : '',
  link : '',
  tags : '',
  comment : ''
}

let CurrentHash = "-Initial-"; // hash mark for the API (avoid override)
// Item Utility Functions
// ---------------------------------------------

const itemUtils = {
  show: (value, normalized = false) => {
    let arrMatch = value.match(/^(@)(.*)$/);
    arrMatch = arrMatch ? arrMatch[2] : value;
    return normalized ? arrMatch.toLowerCase().trim() : arrMatch;
  },
  hasLinkHTML: (item) => {
      return item.link && item.link.substring(0, 4) === "http";
  },
  isFirstHeader: (prev, current) => {
    if (!prev) { return true; }
    let prevDeck = itemUtils.show(prev.deck, true);
    let currentDeck = itemUtils.show(current.deck, true);
    let prevHeader = itemUtils.show(prev.header, true);
    let currentHeader = itemUtils.show(current.header, true);
    return prevDeck !== currentDeck || prevHeader !== currentHeader;
  },
  normalizeItem: (item) => {
    return {
      deck: item.deck,
      header: itemUtils.show(item.header, true),
      subheader: itemUtils.show(item.subheader, true),
      article: itemUtils.show(item.article, true)
    }
  },
  comparePrev: (list, ndx, prop) => {
    if (ndx === 0) { return false; }
    const str1 = list[ndx][prop].trim().toLowerCase().replace(/^@/,'')
    const str2 = list[ndx-1][prop].trim().toLowerCase().replace(/^@/,'')
    return str1 === str2
  },
  isMatching: (item) => {

    // First, apply the advanced search
    const fields = [];
    for (const property in searchParams) {
      fields.push (property);
    }

    for (let i=0; i< fields.length; i++) {
      if (searchParams[fields[i]]) {
        const filter = escapeRegex(searchParams[fields[i]]);
        const searchExpr = new RegExp(filter, "i");
        if (!searchExpr.test(item[fields[i]])) {
          return false;
        }
      }
    }

    // Then see if the item match the input value.
    const searchInput = document.querySelector("#search");
    const filter = escapeRegex(searchInput.value);
    const searchExpr = new RegExp(filter, "i");

    const isSearchInputMatch = fields.find (field => searchExpr.test(item[field]));

    return !!isSearchInputMatch;

  },
  sortFnc: (itemA, itemB) => {
    let a = itemUtils.normalizeItem(itemA);
    let b = itemUtils.normalizeItem(itemB);
    if (a.deck !== b.deck) { return (a.deck > b.deck ? 1 : -1); }
    if (a.header !== b.header) { return (a.header > b.header ? 1 : -1); }
    if (a.subheader !== b.subheader) { return (a.subheader > b.subheader ? 1 : -1); }
    if (a.article !== b.article) { return (a.article > b.article ? 1 : -1); }
    return 0;
  }
}

function advancedSearch (event) {
  event.preventDefault();

  const modal = getModalParentElement (event.target);

  const fields = modal.querySelectorAll("input, select");
  fields.forEach ( field => {
    if (field.name !== "not_in_use") {
      searchParams[field.name] = field.value;
    }
  })

  filteredItems = allItems;
  filterItems();
  showSearchPills();
  gotoPage(1);
}

function clearAdvancedSearch (event) {

  const modal = getModalParentElement (event.target);

  const fields = modal.querySelectorAll("input, select");
  fields.forEach ( field => {
    searchParams[field.name] = "";
    field.value = "";
  })
}

function processItemsToRows() {

  const entryData = document.querySelector(".entry-data");

  removeAllChildNodes(entryData);

  const deckTemplate = getTemplate("deck-row-template");
  const headerTemplate = getTemplate("header-row-template");
  const subheaderTemplate = getTemplate("subheader-row-template");
  const articleTemplate = getTemplate("article-row-template");

  const rows = [];

  if (pagination.totalItemCount === 0) {
    const html = `<div class="no-match-row">Sorry, no matching items</div>`
    entryData.insertAdjacentHTML("afterbegin", html);
    return;
  }

  for (var i = pagination.firstItem; i <= pagination.lastItem; i++) {

    displayItem = {
      id: filteredItems[i]._id.$oid,
      deck : itemUtils.show(filteredItems[i].deck),
      header : itemUtils.show(filteredItems[i].header),
      subheader : itemUtils.show(filteredItems[i].subheader),
      ...icons,
      continue : ""
    }

    if (itemUtils.hasLinkHTML(filteredItems[i])) {
      if (filteredItems[i].article.trim() !== "") {
        link_a = `<a href=${filteredItems[i].link} target="_blank">${filteredItems[i].article}</a>`;
      } else {
        link_a = `<a href=${filteredItems[i].link} target="_blank">${filteredItems[i].link}</a>`;
      }
      displayItem.article = link_a;
    } else {
      displayItem.article = filteredItems[i].article;
    }

    if (filteredItems[i].comment && filteredItems[i].comment.trim().length > 0) {
      displayItem.comment_icon = icons.ICON_COMMENT;
      displayItem.comment = filteredItems[i].comment;
    } else {
      displayItem.comment_icon = "";
      displayItem.comment = "";
    }

    // Add a deck row?
    if (!itemUtils.comparePrev (filteredItems, i, "deck")) {
      rows.push (deckTemplate.supplant(displayItem));
    } else {
      if (i === pagination.firstItem) {
        displayItem.continue = CONTINUE;
        rows.push (deckTemplate.supplant(displayItem));
      }
    }

    // Add a header row?
    if (!itemUtils.comparePrev (filteredItems, i, "header")) {
      rows.push (headerTemplate.supplant(displayItem));
    } else {
      if (i === pagination.firstItem) {
        displayItem.continue = CONTINUE;
        rows.push (headerTemplate.supplant(displayItem));
      }
    }

    // Add a subheader row?
    if (!itemUtils.comparePrev (filteredItems, i, "subheader")) {
      rows.push (subheaderTemplate.supplant(displayItem));
    } else {
      if (i === pagination.firstItem) {
        displayItem.continue = CONTINUE;
        rows.push (subheaderTemplate.supplant(displayItem));
      }
    }

    // Finally, add the item itself
    rows.push (articleTemplate.supplant(displayItem));
  }

  const html = `<div>${rows.join('')}</div>`;

  entryData.insertAdjacentHTML("afterbegin", html);

}

/*
---------------------------------
Item form functions
---------------------------------
*/

function duplicateItem () {
  const modal = document.querySelector("#modal-item");
  const itemId = modal.querySelector("#itemId");
  itemId.value = "";
  colorizeNewItem();
  toggleItemActionButtons();
}

function addItem () {
  const modal = document.querySelector("#modal-item");
  const itemForm = modal.querySelector("form");
  const item = serializeArray(itemForm);
  if (validateItem(item)) {
    const json = JSON.stringify(item);
    const response = httpPost('POST', `${getDomainURL()}${API_URL}`, json);
    refreshPage();
    showSnackbar('Item added', 'success');
  }
}

function updateItem () {
  const modal = document.querySelector("#modal-item");
  const itemForm = modal.querySelector("form");
  const itemId = modal.querySelector("#itemId");
  const item = serializeArray(itemForm);
  if (validateItem(item)) {
    const json = JSON.stringify(item);
    const response = httpPost('PATCH', `${getDomainURL()}${API_URL}/${itemId.value}`, json);
    refreshPage();
    showSnackbar('Item updated', 'success');
  }
}

function deleteItem () {
  const modal = document.querySelector("#modal-item");
  const itemId = modal.querySelector("#itemId");
  const response = httpPost('DELETE', `${getDomainURL()}${API_URL}/${itemId.value}`);
  refreshPage();
  showSnackbar('Item deleted', 'success');
}

function clearEntry() {
  const modal = document.querySelector("#modal-item");
  modal.querySelector("#article").value="";
  modal.querySelector("#link").value="";
  modal.querySelector("#tags").value="";
  modal.querySelector("#comment").value="";
}

function colorizeNewItem() {
  const modal = document.querySelector("#modal-item");
  const itemId = modal.querySelector("#itemId");
  const fields = modal.querySelectorAll("input, textarea, select");
  const isNewItem = (itemId.value === "");

  fields.forEach(field => {
    if (field.name !== "itemId") {
      field.classList.toggle('new-item', isNewItem);
    }
  })
}

function toggleItemActionButtons() {

  const modal = document.querySelector("#modal-item");
  const itemId = modal.querySelector("#itemId");
  const buttons = modal.querySelectorAll("button");
  const isNewItem = (itemId.value === "");

  buttons.forEach (button => {
    if (button.dataset.show) {
      button.classList.remove("hidden"); // Show buttons by defaults
      if ((button.dataset.show === "existingItem" && isNewItem) ||
          (button.dataset.show === "newItem" && !isNewItem)) {
        button.classList.add("hidden");
      }
    }
  })

}

function toggleNeedAPI () {

  const modal = document.querySelector("#modal-item");
  const buttons = modal.querySelectorAll("button");
  const inputs = modal.querySelectorAll("input, textarea, select");

  // If you are not using the API, hide the buttons for it.
  buttons.forEach (button => {
    if (!usingAPI && (button.classList.contains("need-API"))) {
      button.classList.add("hidden");
    }
  });

  // If you are not using the API, set the input in readonly.
  inputs.forEach (input => {
    if (!usingAPI && (input.classList.contains("need-API"))) {
      if (input.type === "text" || input.type === "textarea") {
        input.readOnly = true;
        input.classList.add("inactive");
        input.classList.remove("hidden");
      } else if (input.type === "select-one") {
        input.classList.add("hidden");
      } else if (input.type === "checkbox") {
        input.classList.add("hidden");
      }
    }
  })
}

function toggleTab (eventOrElement) {
  const trigger = (eventOrElement.target ? eventOrElement.target : eventOrElement);
  const showTab = trigger.dataset.tab;
  const tabs = document.querySelectorAll(".entry-tab");
  const tabsBody = document.querySelectorAll(".entry-tab-body");

  tabs.forEach (tab => {
    tab.classList.toggle("active-tab", tab.dataset.tab === showTab);
  });

  tabsBody.forEach (tab => {
    tab.classList.toggle("hidden", tab.dataset.tab !== showTab);
  });
}

/*
---------------------------------
Helper functions
---------------------------------
*/

function refreshPage() {
  try {
    if (usingAPI) {
      strItems = httpGet(`${getDomainURL()}${API_URL}`);
      allItems = JSON.parse(strItems);
    }
  } catch (e) {
    console.log(e)
  }
  allItems.sort(itemUtils.sortFnc);
  filteredItems = allItems;
  filterItems();
  showSearchPills();
  toggleAPIButtons();
  gotoPage();
}

function searchKeyUp () {
  clearTimeout(timeoutId);
  timeoutId = setTimeout(search, 800);
  const searchMsg = document.querySelector("#search-message");
  searchMsg.innerHTML = "Waiting on input...";
}

function search () {
  const searchInput = document.querySelector("#search");
  const searchMsg = document.querySelector("#search-message");
  searchMsg.innerHTML = "";

  if (searchInput.value.length >= 3) {
    searchMsg.innerHTML = "Searching...";
    filterItems();
    showSearchPills();
    gotoPage(1);
  } else {
    if (searchInput.value.length > 0) {
      const searchMsgOverride = document.querySelector("#search-message-override");
      searchMsgOverride.innerHTML = "Please enter at least 3 letters...";
      searchMsg.classList.add("hidden");
      searchMsgOverride.classList.remove("hidden");
      setTimeout(function(){
        searchMsg.classList.remove("hidden");
        searchMsgOverride.classList.add("hidden");
      }, 1000);
      return;
    }
    filteredItems = allItems;
    filterItems();
    showSearchPills();
    gotoPage(1);
  }
}

function filterItems () {
  filteredItems = [];
  for (var i=0; i < allItems.length; i++) {
    if (itemUtils.isMatching(allItems[i])) {
      filteredItems.push(allItems[i]);
    }
  }
  pagination.setItemCount (filteredItems.length);
}

function clearSearch() {
  const searchMsg = document.querySelector("#search-message");
  searchMsg.innerHTML = "";
  const searchInput = document.querySelector("#search");
  searchInput.value = "";
  search();
}

function showMatchCount () {
  const searchMsg = document.querySelector("#search-message");
  searchMsg.innerHTML = `Showing ${pagination.showingCount} of ${pagination.totalItemCount} matching items`;
}

function toggleAPIButtons () {
  const buttons = document.querySelectorAll(".need-API");
  buttons.forEach(function(button) {
    button.classList.toggle('hidden', !usingAPI);
  });
}

function setCommentIcon () {
  const commentTriggers = document.querySelectorAll('.comment-icon');
  // Now, let's add "click" events
  commentTriggers.forEach(function(trigger) {
    trigger.addEventListener('click', function(event) {
      if (trigger.innerHTML === icons.ICON_COMMENT) {
        const commentDiv = trigger.parentElement.nextElementSibling;
        commentDiv.classList.toggle("hidden");
      }
    });
  });
}

function doGroupActions (event) {
  const action = event.target.innerHTML;
  const actions = {
    "Fold All" : () => toggleFoldAll(true),
    /* "Unfold All" : () => toggleFoldAll(false), */
    "Collapse All" : () => toggleCollapseAll(true),
    "Expand All" : () => toggleCollapseAll(false),
  }
  const actionList = [];
  for (const action in actions) { actionList.push(action); }
  const defaultAction = 0;
  const ndx = actionList.reduce((sel, val, ndx) => (val === action ? ndx : sel), defaultAction);
  actions[actionList[ndx]]();
  const nextNdx = ((ndx + 1) % actionList.length);
  event.target.innerHTML = actionList[nextNdx];
}

function setFoldIcon () {
  const triggers = document.querySelectorAll('.group-fold-icon');
  triggers.forEach(trigger => trigger.addEventListener('click', toggleFoldGroup));
}

function toggleFoldAll (isFolding = true) {
  const triggers = document.querySelectorAll('.deck-row .group-fold-icon');
  triggers.forEach(trigger => trigger.innerHTML = (isFolding ? icons.ICON_CLOSE : icons.ICON_OPEN ));
  triggers.forEach(trigger => toggleFoldGroup(trigger));
}

function toggleFoldGroup (eventOrElement) {
  console.log (`collapseGroup: called`);
  const element = (eventOrElement.target ? eventOrElement.target : eventOrElement);
  const isClosing = element.innerHTML === icons.ICON_CLOSE;
  element.innerHTML = (isClosing ? icons.ICON_OPEN : icons.ICON_CLOSE);

  // Find the current group's information
  const group = element.parentElement.parentElement;
  const foldLevel = group.dataset.foldLevel;

  // Address the current group's collapse icon
  const iconCollapse = group.querySelector('.group-collapse-icon');
  iconCollapse.innerHTML = (isClosing ? icons.ICON_COLLAPSE_UP : icons.ICON_COLLAPSE_DOWN);

  // Close the siblings
  let sibling = group.nextElementSibling;
  while(sibling) {
    if (sibling.dataset.foldLevel <= foldLevel) { return; }
    if (isClosing) {
      sibling.classList.add("hidden");
    } else {
      sibling.classList.remove("hidden");
      const icon = sibling.querySelector('.group-fold-icon');
      if (icon) {icon.innerHTML = icons.ICON_CLOSE;}
    }
    if (sibling.dataset.foldLevel !== "4") {
      const iconCollapse = sibling.querySelector('.group-collapse-icon');
      iconCollapse.innerHTML = (isClosing ? icons.ICON_COLLAPSE_UP : icons.ICON_COLLAPSE_DOWN);
    }
    sibling = sibling.nextElementSibling;
  }
}

function setCollapseIcon () {
  const triggers = document.querySelectorAll('.group-collapse-icon');
  triggers.forEach(trigger => trigger.addEventListener('click', toggleCollapseGroup));
}

function toggleCollapseAll (isCollapsing = true) {
  const triggers = document.querySelectorAll('.deck-row .group-collapse-icon');
  triggers.forEach(trigger => trigger.innerHTML = (isCollapsing ? icons.ICON_COLLAPSE_DOWN : icons.ICON_COLLAPSE_UP));
  triggers.forEach(trigger => toggleCollapseGroup(trigger));
}

function toggleCollapseGroup (eventOrElement) {
  const element = (eventOrElement.target ? eventOrElement.target : eventOrElement);
  const isClosing = element.innerHTML === icons.ICON_COLLAPSE_DOWN;

  element.innerHTML = (isClosing ? icons.ICON_COLLAPSE_UP : icons.ICON_COLLAPSE_DOWN);

  const group = element.parentElement.parentElement;
  const foldLevel = group.dataset.foldLevel;

  const groupIconFold = group.querySelector('.group-fold-icon');
  groupIconFold.innerHTML = (isClosing ? icons.ICON_OPEN : icons.ICON_CLOSE);

  let sibling = group.nextElementSibling;
  while(sibling) {
    if (sibling.dataset.foldLevel <= foldLevel) { return; }
    if (sibling.dataset.foldLevel === "4") {
      sibling.classList.toggle("hidden", isClosing);
    } else {
      sibling.classList.remove("hidden");
      const iconCollapse = sibling.querySelector('.group-collapse-icon');
      iconCollapse.innerHTML = (isClosing ? icons.ICON_COLLAPSE_UP : icons.ICON_COLLAPSE_DOWN);
      const iconFold = sibling.querySelector('.group-fold-icon');
      iconFold.innerHTML = (isClosing ? icons.ICON_OPEN : icons.ICON_CLOSE);
    }
    sibling = sibling.nextElementSibling;
  }
}

function setSearchIcon () {
  const searchTriggers = document.querySelectorAll('.filter-icon');
  // Now, let's add "click" events
  searchTriggers.forEach(function(trigger) {
    trigger.addEventListener('click', () => {
      const group = trigger.parentElement.parentElement;
      const groupField = group.dataset.groupField;
      const rowValue = group.querySelector(".row-value");
      searchParams[groupField] = rowValue.innerHTML;
      filterItems();
      showSearchPills();
      gotoPage(1);
    });
  });
}

function setGroupReclassifyIcon () {
  const searchTriggers = document.querySelectorAll('.group-reclassify-icon');
  // Now, let's add "click" events
  searchTriggers.forEach(function(trigger) {
    trigger.addEventListener('click', () => {
      openModal('modal-group-reclassify', trigger);
    });
  });
}

function setGroupDeleteIcon () {
  const searchTriggers = document.querySelectorAll('.group-delete-icon');
  // Now, let's add "click" events
  searchTriggers.forEach(function(trigger) {
    trigger.addEventListener('click', () => {
      openModal('modal-group-delete', trigger);
    });
  });
}

function showSnackbar(message, type) {
  // Get the snackbar DIV
  var snackbar = document.querySelector("#snackbar");

  snackbar.innerHTML = message;
  // Add the "show" class to DIV
  snackbar.className = "show";
  if (type) { snackbar.classList.add(type); }

  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}

function showSearchPills () {
  const top = document.querySelector(".top-row-header");
  let pills = "";
  for (const property in searchParams) {
    if (searchParams[property]) {
      let classes = "search-pill";
      const specialFields = ["deck", "header", "subheader", "article"]

      if (specialFields.includes(property)) {
        classes += ` ${property}-row-color`;
      }
      pills += `
        <div class="${classes}">
          ${property.toUpperCase()} = ${searchParams[property]}
          <span onclick="removeSearchPill('${property}')">${icons.ICON_EXIT}</span>
        </div>`;
    }
  }
  removeAllChildNodes(top);
  top.insertAdjacentHTML("afterbegin", pills);
}

function removeSearchPill (field) {
  searchParams[field] = "";
  filterItems();
  showSearchPills();
  gotoPage(1);
}

/*
----------------------------
Pagination
----------------------------
*/

class Pagination {

  constructor (itemPerPage, windowPageAmount) {

      // Will stay the same while browsing pages
      this.itemPerPage = itemPerPage;
      this.windowPageAmount = windowPageAmount; // How many page number will be shown

      this.totalItemCount = 0;
      this.maxPageNumber = 1;

      // Changes based on the current page
      this.currentPage = 1;
      this.firstItem = 0
      this.lastItem = 0;
      this.showingCount = 0;
      this.windowPageMin = 1; // Min value on the page number shown
      this.windowPageMax = 1; // Max value on the page number shown
      this.prev = false; // Can we go backward?
      this.next = false; // Can we go forward?

  }

  setItemCount (totalItemCount) {
      this.totalItemCount = totalItemCount;
      this.maxPageNumber = Math.max(Math.ceil(totalItemCount / this.itemPerPage), 1);
      this.page = 1;
  }

  setPage (pageNo) {
      if (pageNo == 0) {
          this.currentPage = 1;
      } else if (pageNo > this.maxPageNumber) {
          this.currentPage = this.maxPageNumber;
      } else {
          this.currentPage = pageNo;
      }
      this.recalculate();
  }

  recalculate (itemPerPage = this.itemPerPage) {
      this.itemPerPage = itemPerPage;
      this.maxPageNumber = Math.max(Math.ceil(this.totalItemCount / this.itemPerPage), 1);
      this.firstItem = (this.currentPage - 1) * this.itemPerPage;
      this.lastItem = Math.min(Math.max(this.totalItemCount,1), this.currentPage * this.itemPerPage) - 1;
      this.showingCount = (this.totalItemCount === 0 ? 0 : this.lastItem - this.firstItem + 1);

      // Now let's calculate the pagination window (for the page toolbar)
      const halfPageWindow = Math.floor(this.windowPageAmount / 2);
      this.windowPageMin = Math.max(this.currentPage - halfPageWindow, 1);
      this.windowPageMax = Math.min(this.windowPageMin + this.windowPageAmount - 1, this.maxPageNumber);

      // Let's try to keep the widest window we can keep...
      if (this.windowPageMax - this.windowPageMin + 1 < this.windowPageAmount) {
          if (this.windowPageMin !== 1 || this.windowPageMax !== this.maxPageNumber) {
              if (this.windowPageMin !== 1) {
                  this.windowPageMin = Math.max(1, this.windowPageMax - this.windowPageAmount + 1);
              } else {
                  this.windowPageMax = Math.min(this.windowPageMin + this.windowPageAmount, this.maxPageNumber);
              }
          }
      }

      // Can we move forward and backward?
      this.prev = this.currentPage > 1;
      this.next = this.currentPage < this.maxPageNumber;
  }

}

function showPagination () {

  const ellipsisButtonTemplate = `<button class="page-button inactive page-ellipsis">...</button>`;
  const inactiveButtonTemplate = `<button class="page-button inactive {classes}">{anchor}</button>`;
  const activeButtonTemplate = `<button class="page-button {classes}" onclick="gotoPage({gotoPage})"><a href="#">{anchor}</a></button>`;

  const paginators = document.querySelectorAll(".pagination");

  paginators.forEach((pageButtons) => {

    btnInfo = [];

    btnInfo.push ({
      anchor : "&lt;&lt;",
      gotoPage : Math.max(1, pagination.currentPage - PAGEWINDOW),
      classes : "",
      template : (pagination.prev ? activeButtonTemplate : inactiveButtonTemplate)
    });

    btnInfo.push ({
      anchor : "&lt;",
      gotoPage : Math.max(1, pagination.currentPage - 1),
      classes : "",
      template : (pagination.prev ? activeButtonTemplate : inactiveButtonTemplate)
    });

    if (pagination.windowPageMin > 1) {
      btnInfo.push ({
        anchor : "",
        gotoPage : "",
        classes : "",
        template : ellipsisButtonTemplate
      });
    }

    for(let i=pagination.windowPageMin; i <= pagination.windowPageMax; i++) {
      btnInfo.push ({
        anchor : i,
        gotoPage : i,
        classes : (pagination.currentPage === i ? "current-page" : ""),
        template : (pagination.currentPage === i ?  inactiveButtonTemplate : activeButtonTemplate)
      });
    }

    if (pagination.windowPageMax < pagination.maxPageNumber) {
      btnInfo.push ({
        anchor : "",
        gotoPage : "",
        classes : "",
        template : ellipsisButtonTemplate
      });
    }

    btnInfo.push ({
      anchor : "&gt;",
      gotoPage : Math.min(pagination.maxPageNumber, pagination.currentPage + 1),
      classes : "",
      template : (pagination.next ? activeButtonTemplate : inactiveButtonTemplate)
    });

    btnInfo.push ({
      anchor : "&gt;&gt;",
      gotoPage : Math.min(pagination.maxPageNumber, pagination.currentPage + PAGEWINDOW),
      classes : "",
      template : (pagination.next ? activeButtonTemplate : inactiveButtonTemplate)
    });

    removeAllChildNodes(pageButtons);

    const buttons = btnInfo.map((button) => button.template.supplant(button));
    pageButtons.insertAdjacentHTML("afterbegin", buttons.join(""));

  });

}

function gotoPage (pageNumber) {
  if (pageNumber === undefined) {
    const lsPageNumber = localStorage.getItem('current_page')
    pageNumber = (lsPageNumber ? parseInt(lsPageNumber) : 1);
    const max = pagination.maxPageNumber;
    if(pageNumber > max) { pageNumber = max; }
  }
  localStorage.setItem('current_page', pageNumber);
  pagination.setPage (pageNumber);
  processItemsToRows ();
  setFoldIcon();
  setCollapseIcon();
  setSearchIcon ();
  setCommentIcon ();
  setGroupReclassifyIcon();
  setGroupDeleteIcon();
  closeModals ();
  showMatchCount ();
  showPagination ();
}

/*
----------------------------
Modals
----------------------------
*/

const modalFnc = {}; // Modal function (pre-loaders, etc.)

function createModal(modalId, preLoadFnc) {
  const template = document.querySelector('#modal-template');
  const modal = template.content.cloneNode(true);
  const article = modal.querySelector("article");
  article.id = modalId;
  if (preLoadFnc) {
    article.dataset.preLoadFnc = preLoadFnc;
  }
  document.body.appendChild(modal);
}

function openModal(modalId, event) {
  const modal = document.querySelector(`#${modalId}`);
  const preLoadFnc = modal.dataset.preLoadFnc;
  if (preLoadFnc && modalFnc[preLoadFnc]) {
      modalFnc[preLoadFnc](event);
  }
  modal.classList.add('open');
}

function closeModals () {
  const modals = document.querySelectorAll(".modal");
  modals.forEach(function(modal) {
    modal.classList.toggle('open', false);
  });
}

function getModalParentElement (element) {
  while (!element.classList.contains("modal")) {
    element = element.parentElement;
  }
  return element;
}

/* -=-=-=-=-=-=-=--=-=-= *
/*  Custom modal function (pre-loaders, etc.) */
/* -=-=-=-=-=-=-=--=-=-= */

modalFnc.fillItemForm = (event) => {
  const trigger = event.target;

  const modal = document.querySelector("#modal-item");
  const mainElement = modal.querySelector("main");
  const headerElement = modal.querySelector("header");

  // Add the title in the header
  removeAllChildNodes(headerElement);
  headerElement.insertAdjacentHTML("afterbegin", `Item Modification`);

  const template = getTemplate("item-template");

  let item = filteredItems.find (x => x._id.$oid === trigger.id)

  if (!item) {
    item = getNewItem();
  }

  // Build the body of the modal
  let displayItem = { ...item };
  displayItem.id = item._id.$oid;
  displayItem.createdAt = displayDate(item.createdAt.$date);
  displayItem.updatedAt = displayDate(item.updatedAt.$date);
  const htmlItem = template.supplant( displayItem );

  // Insert to the main content
  removeAllChildNodes(mainElement);
  mainElement.insertAdjacentHTML("afterbegin", htmlItem);

  // Build the data hierarchy for the dropdown options
  buildDataHierarchy(filteredItems, itemsHierarchy);

  deckSelectionFill(mainElement, item);
  headerSelectionFill(mainElement, item);
  subheaderSelectionFill(mainElement, item);

  // Change the background color of input when it's a new item
  colorizeNewItem();

  // Hide/show active buttons
  toggleItemActionButtons();

  // Show/hide button and set field to readonly (if not using API)
  toggleNeedAPI();

  // Activate the classification tab if it's a new item
  if (displayItem.id === "") {
    const classificationTab = modal.querySelector(`[data-tab="classification"]`);
    toggleTab (classificationTab);
  }

}

modalFnc.fillAdvancedSearchForm = (event) => {
  const modal = document.querySelector("#modal-advanced-search");
  const mainElement = modal.querySelector("main");
  const headerElement = modal.querySelector("header");

  // Add the title in the header
  removeAllChildNodes(headerElement);
  headerElement.insertAdjacentHTML("afterbegin", `Advanced Search`);

  const template = getTemplate("advanced-search-template");

  const htmlItem = template.supplant( searchParams );

  removeAllChildNodes(mainElement);
  mainElement.insertAdjacentHTML("afterbegin", htmlItem);

  // Build the data hierarchy for the dropdown options
  buildDataHierarchy(allItems, itemsHierarchy);

  let emptyValues = { deck: "", header: "", subheader: "" };

  // We need to fill with all possible values.
  deckSelectionFill(mainElement, emptyValues);
  headerSelectionFill(mainElement, emptyValues);
  subheaderSelectionFill(mainElement, emptyValues);

  // Now we need to set the values manually
  const setFieldValue = field => {
    if (searchParams[field] !== "") {
      const cbo = getComboFields(modal, field);
      if (isSelectOption(cbo.select, searchParams[field])) {
        cbo.select.value = searchParams[field];
        cbo.input.value = searchParams[field];
      } else {
        cbo.select.value = "";
        cbo.input.value = searchParams[field];
        toggleCombo(cbo.combo, true);
      }
    }
  }

  setFieldValue ("deck");
  setFieldValue ("header");
  setFieldValue ("subheader");

  // Set focus on the first field. We need to wait for the dialog to show.
  setTimeout (() => {
    modal.querySelector(`[name="deck"]`).focus();
  }, 300);

}

modalFnc.fillGroupReclassifyForm = (trigger) => {
  const modal = document.querySelector("#modal-group-reclassify");
  const mainElement = modal.querySelector("main");
  const headerElement = modal.querySelector("header");

  const group = trigger.parentElement.parentElement;
  const groupField = group.dataset.groupField;
  const groupLabel = groupField.charAt(0).toUpperCase() + groupField.slice(1);

  // Add the title in the header
  removeAllChildNodes(headerElement);

  const html = `<div>${groupLabel} Reclassification</div>`;
  headerElement.insertAdjacentHTML("afterbegin", html);

  // Build the data hierarchy for the dropdown options
  buildDataHierarchy(filteredItems, itemsHierarchy);

  // Build the body of the modal
  const groupValue = group.querySelector(".row-value").innerHTML;

  let values = {
    groupField,
    groupValue,
    affectedCount: 0,
    deck: "", header: "", subheader: ""
  };

  if (groupField === "deck") {
    values.deck = groupValue;
  } else if (groupField === "header") {
    values.deck =  group.dataset.deckValue;
    values.header = groupValue;
  } else if (groupField === "subheader") {
    values.deck =  group.dataset.deckValue;
    values.header =  group.dataset.headerValue;
    values.subheader = groupValue;
  }
  values.affectedCount = countGroupItem (filteredItems, values);

  const template = getTemplate("group-reclassify-template");
  const htmlItem = template.supplant( values );

  // Insert to the main content
  removeAllChildNodes(mainElement);
  mainElement.insertAdjacentHTML("afterbegin", htmlItem);

  // showing and filling combo boxes
  const fieldList = ["deck", "header", "subheader"];
  const showFields = fieldList.slice(0, fieldList.indexOf(groupField)+1);
  const cboFills = {
    "deck": deckSelectionFill,
    "header" : headerSelectionFill,
    "subheader" : subheaderSelectionFill
  }

  fieldList.forEach (field => {
    const isShowing = showFields.includes(field);
    const fieldFrom = modal.querySelector(`[data-group-from="${field}"]`);
    const fieldInput = modal.querySelector(`[data-group-field="${field}"]`);
    if (isShowing) {
      fieldFrom.classList.remove("hidden");
      fieldInput.classList.remove("hidden");
      cboFills[field](mainElement, values);
    } else {
      fieldFrom.classList.add("hidden");
      fieldInput.classList.add("hidden");
    }
  })

}

modalFnc.fillGroupDeleteForm = (trigger) => {
  const modal = document.querySelector("#modal-group-delete");
  const mainElement = modal.querySelector("main");
  const headerElement = modal.querySelector("header");

  const group = trigger.parentElement.parentElement;
  const groupField = group.dataset.groupField;
  const groupLabel = groupField.charAt(0).toUpperCase() + groupField.slice(1);

  // Add the title in the header
  removeAllChildNodes(headerElement);
  headerElement.insertAdjacentHTML("afterbegin", `${groupLabel} Delete`);

  // Build the body of the modal
  const groupValue = group.querySelector(".row-value").innerHTML;

  const values = { groupLabel, groupValue, deck: "", header: "", subheader: "", affectedCount: 0 };

  if (groupField === "deck") {
    values.deck = groupValue;
  } else if (groupField === "header") {
    values.deck =  group.dataset.deckValue;
    values.header = groupValue;
  } else if (groupField === "subheader") {
    values.deck =  group.dataset.deckValue;
    values.header =  group.dataset.headerValue;
    values.subheader = groupValue;
  }
  values.affectedCount = countGroupItem (filteredItems, values);

  const template = getTemplate("group-delete-template");

  const htmlItem = template.supplant( values );

  // Insert to the main content
  removeAllChildNodes(mainElement);
  mainElement.insertAdjacentHTML("afterbegin", htmlItem);

}


/*
----------------------------
Miscellaneous helpers
----------------------------
*/

function httpGet(theUrl) {
  try {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open("GET", theUrl, false);
    xmlHttpReq.setRequestHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    xmlHttpReq.setRequestHeader('Pragma', 'no-cache');
    xmlHttpReq.setRequestHeader('Expires', '0');
    xmlHttpReq.send(null);
    CurrentHash = xmlHttpReq.getResponseHeader("current-hash");
    return xmlHttpReq.response;
  } catch(e) {
    throw `Nope, not going to work: ${e.message}`;
  }
}

function httpPost(method, theUrl, data) {
  try {
    let xmlHttpReq = new XMLHttpRequest();
    xmlHttpReq.open(method, theUrl, false);
    xmlHttpReq.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    xmlHttpReq.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlHttpReq.setRequestHeader('current-hash', CurrentHash);
    xmlHttpReq.setRequestHeader('Accept', 'application/json');
    xmlHttpReq.send(data);
    if (xmlHttpReq.status === 200) {
      CurrentHash = xmlHttpReq.getResponseHeader("current-hash");
    } else if (xmlHttpReq.status === 400) { //Expecting a Hash issue...
      const resp = JSON.parse(xmlHttpReq.response);
      if (resp.message) {
        showSnackbar(resp.message, "error");
      }
      throw JSON.parse(xmlHttpReq.response);
    } else { // Some other error
      throw xmlHttpReq.response;
    }
    return xmlHttpReq.response;
  } catch(e) {
    throw `Nope, not going to work: ${e.message}`;
  }
}

function displayDate (myDate) {
  var date = new Date(myDate);
  return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`
}

function getTemplate (templateName) {
  const template = document.querySelector(`#${templateName}`).innerHTML;
  return template.replaceAll(/\n/g,'').trim();
}

function serializeArray(form) {
  const ignoreFields = ["itemId", "not_in_use"];
  const objects = {};
  if (typeof form == 'object' && form.nodeName.toLowerCase() == "form") {
    const fields = form.querySelectorAll("input, textarea, select");
    for(let i=0; i < fields.length; i++){
      if (!ignoreFields.includes(fields[i].getAttribute("name"))) {
        objects[fields[i].getAttribute("name")] = fields[i].value;
      }
    }
  }
  return objects;
}

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
  }
}

function escapeRegex(string) {
  return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

function isUsingServer () {

  if (location.protocol === "file:") { return false; }

  try {
    const resp = httpGet(`${getDomainURL()}/api/alive`);
    return resp.includes ("alive and kicking");
  } catch(e) {
    return false;
  }
}

function getDomainURL () {
  let currentURL = new URL(window.location.href);
  return currentURL.origin;
}

function makeBackup() {
  const bkpURL = `${getDomainURL()}/api/backup`;
  const strResp =  httpGet(bkpURL);
  const response = JSON.parse(strResp);
  showSnackbar(response.message, 'success');
}

function getNewItem () {

  return {
    "_id": { "$oid": "" },
    "deck": "",
    "header": "",
    "subheader": "",
    "caption": "",
    "article": "",
    "link": "",
    "tags": "",
    "comment": "",
    "owner": { "$oid": "" },
    "createdAt": { "$date": `${(new Date()).toJSON()}` },
    "updatedAt": { "$date": `${(new Date()).toJSON()}` }
  }
}

/*
Build the following structure for the drop down in the item modal:

{
  "_ALL_HEADERS_": Set(x) ["Brewery", ...],
  "_ALL_SUBHEADERS_": Set(x) ["San Antonio, TX", ...],
  "_HIERARCHY_": {
    "_ITEM_SET_": Set(x) ["Beer", ...],
    "_ITEM_COUNT_" : 768,
    "Beer": {
      "_ITEM_SET_" : Set(x) ["Brewery", ...],
      "_ITEM_COUNT_" : 13,
      "Brewery" : {
          "_ITEM_SET_" : Set(x) ["San Antonio, TX", ...],
          "_ITEM_COUNT_" : 12,
          "San Antonio, TX" {
            "_ITEM_COUNT_" : 9,
          }
        }
      },
      ...
    },
    ....
  }
}

NOTE: The "list" are Sets not Arrays
NOTE: _HEADERS_BY_DECK_ is an object, not a SET
*/

function countGroupItem (dataArr, values) {
  let count = 0;

  const fieldList = ["deck", "header", "subheader"];
  const fieldToMath = fieldList.filter (field => values[field]); // Get only field with values.

  for (let ndx=0; ndx < dataArr.length; ndx++) {
    const matching = fieldToMath.every (field => dataArr[ndx][field] === values[field]);
    if (matching) { count += 1; }
  }

  return count;
}

function buildDataHierarchy (itemList, hierarchy) {
  // Reinitialize the options
  hierarchy["_HIERARCHY_"] = { "_ITEM_COUNT_" : 0, "_ITEM_SET_" : new Set() };
  hierarchy["_ALL_HEADERS_"] = new Set();
  hierarchy["_ALL_SUBHEADERS_"] = new Set();

  itemList.forEach(item => {
    buildOptionsHierarchy (hierarchy, item.deck, item.header, item.subheader);
  });


  const sortedHeaders = Array.from(hierarchy["_ALL_HEADERS_"]);
  const sortedSubheaders = Array.from(hierarchy["_ALL_SUBHEADERS_"]);

  const sortFnc = (a, b) => (a.toLowerCase().trim() > b.toLowerCase().trim() ? 1 : -1);
  sortedHeaders.sort(sortFnc);
  sortedSubheaders.sort(sortFnc);

  hierarchy["_ALL_HEADERS_"] = new Set (sortedHeaders);
  hierarchy["_ALL_SUBHEADERS_"] = new Set (sortedSubheaders);


}

function buildOptionsHierarchy (hierarchy, deck, header, subheader) {

  hierarchy["_ALL_HEADERS_"].add(header);
  hierarchy["_ALL_SUBHEADERS_"].add(subheader);

  const hierarchyObj = hierarchy["_HIERARCHY_"];
  hierarchyObj["_ITEM_COUNT_"]  += 1;

  const addFilterOption =  (obj, value) => {
    if (!obj["_ITEM_SET_"].has(value)) {
      obj["_ITEM_SET_"].add(value);
      obj[value] = { "_ITEM_COUNT_" : 0, "_ITEM_SET_" : new Set() };
    }
    obj[value]["_ITEM_COUNT_"] += 1;
    return obj[value];
  }

  // Finally add the deck, header and subheader
  const deckObj = addFilterOption (hierarchyObj, deck);
  const headerObj = addFilterOption (deckObj, header);
  addFilterOption (headerObj, subheader);

}

function toggleCombo(eventOrElement, forceText) {
  //We can pass an event of an element directly
  const trigger = (eventOrElement.target ? eventOrElement.target : eventOrElement);

  const field = trigger.dataset.comboField;
  const currentInputType = (forceText ? "select" : trigger.value);

  const modal = getModalParentElement (trigger, field);
  const fields = getComboFields (modal, field);

  if (currentInputType === "text") { // Going from an INPUT to a SELECT

    fields.select.classList.remove("hidden");
    fields.select.name = field;
    fields.select.value = (isSelectOption(fields.select, fields.input.value) ? fields.input.value : "");

    fields.input.classList.add("hidden");
    fields.input.name = "not_in_use";

    trigger.value = "select";

  } else { // Going from a SELECT to an INPUT

    fields.select.classList.add("hidden");
    fields.select.name = "not_in_use";

    fields.input.classList.remove("hidden");
    fields.input.name = field;
    if ((fields.select.value !== "") && !forceText) {
      fields.input.value = fields.select.value;
    }

    trigger.value = "text";
    trigger.checked = true;

    // Let also toggle the "downstream" combos
    if (field !== "subheader") {
      const modal = getModalParentElement(trigger);
      if (field === "deck") {
        const comboHeader = modal.querySelector('input[data-combo-field="header"]');
        toggleCombo (comboHeader, true);
      } else if (field === "header") {
        const comboSubheader = modal.querySelector('input[data-combo-field="subheader"]');
        toggleCombo(comboSubheader, true);
      }
    }

  }

}

function getComboFields (modal, field) {
  const combo = modal.querySelector(`input[data-combo-field="${field}"]`);
  let container = combo.parentElement;
  while (container.children.length < 3) { container = container.parentElement; }
  const select = container.querySelector("select");
  const input = container.querySelector(`input[type="text"]`);
  return { combo, select, input }
}

function deckSelectionChanged (event) {

  const modal = getModalParentElement (event.target);
  const deckCbo = getComboFields(modal, "deck");
  const headerCbo = getComboFields(modal, "header");

  headerCbo.select.value = "";

  headerSelectionFill (modal, { deck: deckCbo.select.value, header : "", subheader : "" } );

  headerSelectionChanged(event);

}

function headerSelectionChanged (event) {

  const modal = getModalParentElement (event.target);

  const deckCbo = getComboFields(modal, "deck");
  const headerCbo = getComboFields(modal, "header");
  const subheaderCbo = getComboFields(modal, "subheader");

  subheaderCbo.select.value = "";

  const item = { deck: "", header : "", subheader : "" }

  // Get the value for the deck
  item.deck = (deckCbo.combo.value === "text" ? deckCbo.input.value : deckCbo.select.value);

  headerCbo.input.value = headerCbo.select.value;
  item.header = headerCbo.select.value;

  headerSelectionFill (modal, item);
  subheaderSelectionFill (modal, item);

}

function subheaderSelectionChanged (event) {
  const modal = getModalParentElement (event.target);
  const subheaderCbo = getComboFields(modal, "subheader");
  subheaderCbo.input.value = subheaderCbo.select.value;
}

function deckSelectionFill (rootElement, item) {

  const modal = getModalParentElement (rootElement);

  const deckCbo = getComboFields(modal, "deck");

  removeAllChildNodes(deckCbo.select);
  deckCbo.select.insertAdjacentHTML("beforeend", blankOption);

  const deckOptions = itemsHierarchy["_HIERARCHY_"]["_ITEM_SET_"];
  deckOptions.forEach(deck => {
    const option = `<option value="${deck}">${deck}</option>`;
    deckCbo.select.insertAdjacentHTML("beforeend", option);
  });
  deckCbo.select.value = item.deck;
}

function headerSelectionFill (rootElement, item) {

  const modal = getModalParentElement (rootElement);

  const headerCbo = getComboFields(modal, "header");

  removeAllChildNodes(headerCbo.select);
  headerCbo.select.insertAdjacentHTML("beforeend", blankOption);

  let headerOptions;
  if (item.deck) {
    headerOptions = itemsHierarchy["_HIERARCHY_"][item.deck]["_ITEM_SET_"];
  } else {
    headerOptions = itemsHierarchy["_ALL_HEADERS_"];
  }
  headerOptions.forEach(header => {
    const option = `<option value="${header}">${header}</option>`;
    headerCbo.select.insertAdjacentHTML("beforeend", option);
  });
  headerCbo.select.value = item.header;
}

function subheaderSelectionFill (rootElement, item) {

  const modal = getModalParentElement (rootElement);

  const subheaderCbo = getComboFields(modal, "subheader");

    removeAllChildNodes(subheaderCbo.select);
    subheaderCbo.select.insertAdjacentHTML("beforeend", blankOption);

    let subheaderOptions;
    if (item.deck && item.header) {
      subheaderOptions = itemsHierarchy["_HIERARCHY_"][item.deck][item.header]["_ITEM_SET_"];
    } else {
      subheaderOptions = itemsHierarchy["_ALL_SUBHEADERS_"];
    }
    subheaderOptions.forEach(subheader => {
      const option = `<option value="${subheader}">${subheader}</option>`;
      subheaderCbo.select.insertAdjacentHTML("beforeend", option);
    });
    subheaderCbo.select.value = item.subheader;
}

function reclassifyGroup (event) {

  const modal = getModalParentElement (event.target);

  const groupFieldInput = modal.querySelector(`input[name="groupField"]`);
  const groupField = groupFieldInput.value;

  const prevDeckInput = modal.querySelector(`input[name="prev_deck"]`);
  const prevHeaderInput = modal.querySelector(`input[name="prev_header"]`);
  const prevSubheaderInput = modal.querySelector(`input[name="prev_subheader"]`);

  const prevValues = {
    deck: prevDeckInput.value,
    header: prevHeaderInput.value,
    subheader: prevSubheaderInput.value
  }

  // NOTE: Only the fields we need to match will have a "previous" value
  const fieldList = ["deck", "header", "subheader"];
  const fieldToMath = fieldList.slice(0, fieldList.indexOf(groupField)+1);

  console.log (`fieldToMath: ${fieldToMath}`)
  const updates = [];

  filteredItems.forEach(item => {
    const matching = fieldToMath.every (field => item[field] === prevValues[field]);
    if (matching) {
      const update = {  "_id" : item._id }
      fieldToMath.forEach(field => {
        const inputField = modal.querySelector(`input[name="${field}"], select[name="${field}"]`);
        update[field] = inputField.value;
      })
      updates.push(update);
    }
  })

  const json = JSON.stringify(updates);
  const response = httpPost('PATCH', `${getDomainURL()}${API_URL}/group`, json);
  refreshPage();
  showSnackbar('Item updated', 'success');
}

function deleteGroup(event) {

  const modal = getModalParentElement (event.target);

  const deckInput = modal.querySelector(`input[name="deck"]`);
  const headerInput = modal.querySelector(`input[name="header"]`);
  const subheaderInput = modal.querySelector(`input[name="subheader"]`);

  const values = {
    deck: deckInput.value,
    header: headerInput.value,
    subheader: subheaderInput.value
  }

  // NOTE: Only the fields we need to match will have a value
  const fieldList = ["deck", "header", "subheader"];
  const fieldToMath = fieldList.filter (field => values[field]); // Get only field with values.

  const deletes = [];

  for (let ndx=0; ndx < filteredItems.length; ndx++) {
    const item = filteredItems[ndx];
    const matching = fieldToMath.every (field => item[field] === values[field]);
    if (matching) {
      const deleteItem = { "_id" : { "$oid" : item._id["$oid"]} };
      deletes.push(deleteItem);
    }
  }

  const json = JSON.stringify(deletes);
  const response = httpPost('DELETE', `${getDomainURL()}${API_URL}/group`, json);
  refreshPage();
  showSnackbar('Items deleted', 'success');
}

function isSelectOption (select, value) {
  const options = select.options;
  const option = Array.from(options).find(option => {
    return (option.value === value);
  });
  return !!option;
}

function validateItem (item) {
  const isInvalid = ["deck", "header", "subheader"].find(x => item[x] === "");
  if (isInvalid) {
    showSnackbar('You must set the Deck, Header and Subheader (see Classification tab)', 'error');
    return false;
  }
  return true;
}

function toggleMenu (){
  const floatNav = document.querySelector('.float-nav');
  floatNav.addEventListener('click', function() {
    const toggles = document.querySelectorAll('.main-nav, .menu-btn');
    toggles.forEach(toggle => toggle.classList.toggle('active'))
  })
}

function toggleCollapseMenu (){
    const toggles = document.querySelectorAll('.collapse-nav, .collapse-menu-btn');
    toggles.forEach(toggle => toggle.classList.toggle('active'))
}

// ---------------------------------------------
// Function used for template value replacement
// Given an object o, replace {var} with value of
// the property o.var for any string or number
// Thanks to Douglas Crockford!
// ---------------------------------------------
String.prototype.supplant = function (o) {
  return this.replace(
      /{([^{}]*)}/g,
      function (a, b) {
          var r = o[b];
          return typeof r === 'string' || typeof r === 'number' ? r : a;
      }
  );
};

// ---------------------------------------------