class Item {
    _id = { $oid : ""};

    deck = "";
    header = "";
    subheader = "";
    article = "";
    link = "";
    tags = "";
    comment = "";

    createdAt = { $date : "" }
    updatedAt = { $date : "" }

    getId () {
        return this._id.$oid;
    }

    setId (ID) {
        this._id.$oid = ID;
    }

    getCreatedDate () {
        return this.createdAt.$date;
    }

    setCreatedDate (strDate) {
        this.createdAt.$date = strDate;
    }

    getUpdatedDate () {
        return this.updatedAt.$date;
    }

    setUpdatedDate (strDate) {
        this.updatedAt.$date = strDate;
    }

    getClassificationFields () {
        return {
            deck : this.deck,
            header : this.header,
            subheader : this.subheader }
    }

    getModifiableFields () {
        return {
            deck : this.deck,
            header : this.header,
            subheader : this.subheader,
            article : this.article,
            link : this.link,
            tags : this.tags,
            comment : this.comment
        }
    }
}