class ItemUtil {

  createUUID() {
    return 'x'.repeat(24).replace(/[x]/g, function(c) {
       var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
       return v.toString(16);
    });
  }

  addItem (item) {
    item._id = { $oid: this.createUUID() };
    item.createdAt = { "$date": (new Date()).toISOString() };
    item.updatedAt = { "$date": (new Date()).toISOString() };
    return item;
  }

  updateItem(changes, item) {
    for (const change in changes) {
        item[change] = changes[change];
    }
    item.updatedAt = { "$date": (new Date()).toISOString() };
  }

  getItemId(item) {
    return item._id.$oid;
  }

  findItem(id, list) {
    return list.find((item) => item._id.$oid === id);
  }

  findItemNdx(id, list) {
    return list.findIndex((item) => item._id.$oid === id);
  }

}

module.exports = ItemUtil;
