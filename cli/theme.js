const { colorCodes } = require("./console");

const displayTheme = {
    "_id.$oid" : [ colorCodes.Dim, colorCodes.FgWhite ],
    "deck" : [ colorCodes.FgWhite ],
    "header" : [],
    "subheader" : [],
    "article" : [ colorCodes.FgGreen ],
    "link" : [ colorCodes.FgCyan ],
    "createdAt.$date" : [],
    "updatedAt.$date" : [],
    "tags" : [],
    "comment" : [ colorCodes.FgMagenta ]
}

const treeTheme = {
    "deck" : [ colorCodes.FgMagenta ],
    "header" : [ colorCodes.FgBlue ],
    "subheader" : [ colorCodes.FgYellow ],
    "exampleId" : [ colorCodes.Dim, colorCodes.FgWhite ]
}

module.exports = { displayTheme, treeTheme };