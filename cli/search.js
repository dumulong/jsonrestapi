const { searchList, sortList } = require("../utils");
const { displayItems } = require("./displayItems");

async function searchItems (Controller, command) {
    let items = await Controller.getItems();
    if (command.itemId !== "") {
        items = searchList (command.itemId, items);
    } else {
        items = searchList (command.text, items);
    }
    items = items.sort (sortList);
    displayItems(items, command.text, command.options);
}

module.exports = { searchItems };
