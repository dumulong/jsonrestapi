const readline = require('readline');

const colorReset = "\x1b[0m";

let colorCodes = Object.freeze ({
    Reset : "0",
    Bright : "1",
    Dim : "2",
    Underscore : "4",
    Blink : "5",
    Reverse : "7",
    Hidden : "8",

    FgBlack : "30",
    FgRed : "31",
    FgGreen : "32",
    FgYellow : "33",
    FgBlue : "34",
    FgMagenta : "35",
    FgCyan : "36",
    FgWhite : "37",
    FgGray : "90",

    BgBlack : "40",
    BgRed : "41",
    BgGreen : "42",
    BgYellow : "43",
    BgBlue : "44",
    BgMagenta : "45",
    BgCyan : "46",
    BgWhite : "47",
    BgGray : "100"
});

const logLevel = {
    "info" : [colorCodes.FgWhite],
    "warning" : [colorCodes.Bright, colorCodes.FgMagenta],
    "failure" : [colorCodes.FgRed],
    "success" : [colorCodes.FgGreen]
}

function input (rli, prompt, defaultVal) {
    return new Promise((callbackFn, errorFn) => {
        let fullPrompt = prompt + (defaultVal ? ` [${defaultVal}] : ` : ` : `);
        rli.question(fullPrompt, (userInput) => {
            callbackFn(userInput || defaultVal);
        }, () => {
            errorFn();
        });
    });
}

async function ask (question, defaultValue = "") {

    const rli = readline.createInterface ({
        input: process.stdin,
        output : process.stdout
    });

    let answer = await input(rli, question, defaultValue);

    rli.close();
    return answer;
}

async function askMany(questionObject) {

    const rli = readline.createInterface ({
        input: process.stdin,
        output : process.stdout
    });

    let showEditFlags = false;
    for (let question in questionObject) {
        if (questionObject[question] !== "") {
            showEditFlags = true;
        }
    }

    if (showEditFlags) {
        const attn = colorize.warning("ATTN :");
        log.info(`${attn} The values within the square brackets "[]" are the default values that will be used if nothing else is entered.`);
        log.info(`${attn} Entering a space " " will erase the default value.`);
        log.info(`${attn} Edit prefixes: [+] => add, [-] => remove, [*] => add line after, [/] => add line before`);
    }

    for (var question in questionObject){
        let resp =  await input(rli, question, questionObject[question]);
        if (resp.substr(0,1) === "+") { // Add value at the end
            questionObject[question] += resp.substr(1);
        } else if (resp.substr(0,1) === "-") { // Remove value
            questionObject[question] = questionObject[question].replace(resp.substr(1), "");
        } else if (resp.substr(0,1) === "*") { // Add a line after
            questionObject[question] = `${questionObject[question]}\n${resp.substr(1)}`;
        } else if (resp.substr(0,1) === "/") { // Remove value
            questionObject[question] = `${resp.substr(1)}\n${questionObject[question]}`;
        } else {
            questionObject[question] = resp;
        }
    }

    rli.close();
}


function removeColors (line) {
    return line.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{1,4})*)?[0-9A-ORZcf-nqry=><]/g,"");
}

function getColorEscString (colorArray) {
    let combined = "";
    if (typeof colorArray === "string") {
        colorArray = [ colorArray ];
    }
    for (let i=0; i < colorArray.length; i++) {
        combined += (combined === "" ? "" : ";" ) + colorArray[i];
    }
    return combined = "\x1b[" + combined + "m";
}

function paint (colorArr, value) {
    return (value ? `${getColorEscString(colorArr)}${value}${colorReset}` : "");
}

const colorize = {
    "info" : (value) => {
        let color = getColorEscString(logLevel.info);
        return `${color}${value}${colorReset}`;
    },
    "warning" : (value) => {
        let color = getColorEscString(logLevel.warning);
        return `${color}${value}${colorReset}`;
    },
    "failure" : (value) => {
        let color = getColorEscString(logLevel.failure);
        return `${color}${value}${colorReset}`;
    },
    "success" : (value) => {
        let color = getColorEscString(logLevel.success);
        return `${color}${value}${colorReset}`;
    }
}

const log = {
    "info" : (value) => {
        let text = colorize.info(value);
        console.log(text);
    },
    "warning" : (value) => {
        let text = colorize.warning(value);
        console.log(text);
    },
    "failure" : (value) => {
        let text = colorize.failure(value);
        console.log(text);
    },
    "success" : (value) => {
        let text = colorize.success(value);
        console.log(text);
    }
}

module.exports = { colorCodes, colorReset, removeColors, getColorEscString, ask, askMany, paint, log, colorize};