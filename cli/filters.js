const fs = require("fs");
const { askMany, log, colorize } = require("./console");
const { getDepthOption } = require("./args");

const FILTER_FILENAME = "./cli/filters.json";

const filter = {
    "deck" : "",
    "header" : "",
    "subheader" : ""
};

const getFilter = async () => {
    return await readFile(FILTER_FILENAME, { ...filter });
}

const processFilter = async (Controller, command) => {

    if (command.itemId !== "" || command.options.includes("-s")) {
        await setFilter (Controller, command);
    } else if (command.options.includes("-c")){
        await clearFilter();
    } else {
        await displayFilter();
    }
}

const displayFilter = async () => {
    let currentFilter = await getFilter();
    let filterDisplay = "";
    for (const [key, value] of Object.entries(currentFilter)) {
        if (value.trim()) {
            filterDisplay += `\t${key}: ${value}\n`;
        }
    }
    filterDisplay = `Filtered by:\n${filterDisplay || "\tNo filters currently set"}`;
    console.log (`\n${colorize.warning(filterDisplay)}`);
}

const clearFilter = async () => {

    let result = await writeFile(FILTER_FILENAME, { ...filter });

    if (result === "SUCCESS") {
        log.success (`Filter set to: ${JSON.stringify(filter)}`);
    } else {
        log.failure("Unable to set the filters")
    }
}

const setFilter = async (Controller, command) => {

    let item = { ...filter };

    if (command.itemId) {
        let dbItem = await Controller.getItem(command.itemId);
        if (dbItem === null) {
            log.error("Sorry, we can't find this item");
            return;
        }
        const depth = getDepthOption(command.options);
        if (depth >= 1) { item.deck = dbItem.deck; }
        if (depth >= 2) { item.header = dbItem.header; }
        if (depth >= 3) { item.subheader = dbItem.subheader; }
    } else {
        item = await readFile(FILTER_FILENAME, item);
        log.info("Enter a filter (space to erase)");
        await askMany(item);
    }

    let result = await writeFile(FILTER_FILENAME, item);

    if (result === "SUCCESS") {
        log.success (`Filter set to: ${JSON.stringify(item)}`);
    } else {
        log.failure("Unable to set the filters")
    }
}

async function filterItems (items) {
    filters = await getFilter();
    let filtered = items.filter ( item => {
        let include = true;
        for (const field in filters) {
            const search = new RegExp(escapeRegExp(filters[field]), "dgi");
            if ((filters[field].trim()) && !search.test(item[field])) {
                include = false;
            }
        }
        return include;
    })
    return filtered;
}

async function readFile (filename, item) {
    return new Promise((resolve, _) => {
        fs.readFile(filename, function(err, buf) {
            let filters = JSON.parse(buf.toString());
            for (const prop in item) {
                item[prop] = filters[prop];
            }
            resolve(item);
        });
    });
}

async function writeFile (filename, item) {

    let data  = JSON.stringify(item);

    return new Promise((resolve, _) => {
        fs.writeFile(filename, data, (err) => {
            if (err) log.failure(err);
            resolve("SUCCESS");
        });
    });
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

module.exports = { processFilter, getFilter, filterItems };