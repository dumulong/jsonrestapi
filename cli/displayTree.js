const { paint, log } = require("./console");
const { filterItems } = require("./filters");
const { treeTheme } = require("./theme");
const { sortList } = require("../utils");
const { getDepthOption } = require("./args");

class Item {
    list =  [];
    count = 0;
    displayed = false;
    exampleId = "";
    constructor(label) {
        this.label = label;
    }
    addChild (child, id) {
        let item = this.list.find(x => {
            return x.label.toLowerCase().trim() === child.toLowerCase().trim()
        })
        if (!item) {
            let newItem = new Item(child);
            newItem.count += 1;
            newItem.exampleId = id;
            this.list.push (newItem);
            return newItem;
        } else{
            item.count += 1;
            return item;
        }
    }
}

const displayTree = async (Controller, command) => {

    let options = parseOptions(command.options);

    let items = await Controller.getItems();
    if (options.filtered) {
        items = await filterItems(items);
    }
    items = items.sort(sortList);

    const tree = new Item("all");

    items.forEach(item => {

        let include = true;

        if (command.text) {
            const search = new RegExp(command.text, "dgi");
            const values = item.deck + " | " + item.header + " | " + item.subheader;
            if (!search.test(values)) {
                include = false;
            }
        }

        if (include) {
            tree.count += 1;
            let deck = tree.addChild(item.deck, item._id.$oid);
            let header = deck.addChild(item.header, item._id.$oid);
            header.addChild(item.subheader, item._id.$oid);
        }


    });

    if (options.maxDepth > 0) {
        console.log("");
        walkTree(tree.list, 0, options);
    }

    let itemCount = `\n${tree.count} item(s) found\n`;
    if (options.colorize) {
        log.success(itemCount);
    } else {
        console.log(itemCount)
    }

}

function walkTree (itemList, depth, options) {
    if (itemList.length > 0) {
        let child = itemList.shift();
        displayItem (child, depth, options);
        if (depth < options.maxDepth -1) {
            walkTree (child.list, depth + 1, options);
        }
        walkTree (itemList, depth, options);
    }
}

function displayItem (item, depth, options) {
    item.displayed = true;
    let label = item.label;
    if (options.colorize) {
        let themeFields = [ "deck", "header", "subheader" ];
        label = paint(treeTheme[themeFields[depth]], item.label);
    }
    label += ` [${item.count}]`;
    if (depth === (options.maxDepth - 1)) {
        label += paint(treeTheme.exampleId, ` -> ${item.exampleId}`);
    }
    console.log (`${"\t".repeat(depth)}${label}`);
}

function parseOptions (optionArr) {
    const options = {};
    options.maxDepth = getDepthOption(optionArr);
    options.colorize = optionArr.includes("-c");
    options.filtered = optionArr.includes("-f");
    return options;
}

module.exports = { displayTree };