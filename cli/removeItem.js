const { ask, colorize, log } = require("./console");

const removeItem = async (Controller, command) => {

    let ID = command.itemId;

    while (!ID) {
        ID = await ask ("ID");
        if (!ID) {
            log.info("Sorry, you need to input the ID");
        }
    }

    let dbItem = await Controller.getItem(ID);
    if (dbItem === null) {
        log.error ("Sorry, we can't find this item");
        return;
    }

    log.warning(`We will be deleting the item [${ID}]: `);
    console.log (dbItem);

    let isOkay = await ask (colorize.warning("Is that correct (y/n)"), "n");

    if (isOkay.toLowerCase() === "y") {
        await Controller.deleteItem(ID, "bypass");
        log.success (`The item has been removed`);
    } else {
        log.failure ("The item will *NOT* be removed");
    }
}

module.exports = { removeItem };