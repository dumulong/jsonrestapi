const { ask, askMany, colorize, log } = require("./console");
const { getFilter } = require("./filters");

const addItem = async (Controller, command) => {

    const item = {
        "deck" : "",
        "header" : "",
        "subheader" : "",
        "article" : "",
        "link" : "",
        "tags" : "",
        "comment" : ""
    }

    if (command.itemId !== "") {

        let dbItem = await Controller.getItem(command.itemId);
        if (dbItem === null) {
            log.error ("Sorry, we can't find this item");
            return;
        }

        for (const prop in item) {
            item[prop] = dbItem[prop];
        }

    } else if (command.options.includes("-f")) {

        let filters = await getFilter();
        for (const prop in filters) {
            item[prop] = filters[prop];
        }

    }

    await askMany(item);

    log.warning("We will be adding:");

    console.log (item);

    let isOkay = await ask (colorize.warning("Is that correct (y/n)"), "n");

    if (isOkay.toLowerCase() === "y") {
        let newItem = await Controller.createItem(item, "bypass");
        log.success (`The item has been added : ${newItem._id.$oid}`);
    } else {
        log.failure ("The item will *NOT* be added");
    }
}

module.exports = { addItem };