const { displayTheme }  = require("./theme");
const { filterItems, getFilter } = require("./filters");
const { colorCodes, colorReset, removeColors, getColorEscString, log, paint, colorize } = require("./console");

async function displayItems (items, searchFor, options) {

    let filters = null;

    if (options.includes("-f")) {
        filters = await getFilter();
        items = await filterItems(items);
    }

    let now = new Date();
    let tag = `${("0" + now.getHours()).slice(-2)}:${("0" + now.getMinutes()).slice(-2)}:${("0" + now.getSeconds()).slice(-2)}`;
    let separatorColor = [ colorCodes.FgRed, colorCodes.Bright ];
    let tagColor = [ colorCodes.FgWhite ];

    let separator = `${paint(separatorColor, "=".repeat(65))}\n`;
    separator += `${paint(separatorColor, "=".repeat(1))} ${paint(tagColor, tag)} XXX ${paint(separatorColor, "=".repeat(50))}\n`
    separator += `${paint(separatorColor, "=".repeat(65))}`;

    let open = separator.replace("XXX", "BEG");
    let close = separator.replace("XXX", "END");

    let lines = [];

    lines.push(`\n${open}`);

    items.forEach((item, index) => {

        if (!options.includes("-c") && index !== 0) {
            // No colors requested, at least separate with dashes
            lines.push(`\n${"-".repeat(10)}`);
        }

        lines.push(`\n${paint([colorCodes.Dim, colorCodes.FgWhite], "ID")} : ${printField("_id.$oid", item)}`);

        lines.push(`${printField("deck", item)} | ${printField("header", item)} | ${printField("subheader", item)}`);

        let article = printField("article", item);
        let link = printField("link", item);
        let info = article + (article && link ? " : " : "") + link;
        lines.push(info);

        if (options.includes("-v")) {
            lines.push (`Tags: ${printField("tags", item)}`);
            lines.push ("Comment: ");
            if (item.comment.trim()) {
                lines.push (printField("comment", item));
            }
            lines.push (`Created: ${printField("createdAt.$date", item)}`);
            lines.push (`Updated: ${printField("updatedAt.$date", item)}`);
        }

        for (let i=0; i < lines.length; i++) {
            lines[i] = highlightWord(searchFor, lines[i])
        }

    });

    lines.push (`\n${close}`);

    if (options.includes("-f")) {
        let filterWarning = "";
        for (const [key, value] of Object.entries(filters)) {
            if (value.trim()) {
                filterWarning += `\t${key}: ${value}\n`;
            }
        }
        if (filterWarning) {
            filterWarning = `Filtered by:\n${filterWarning}`
        }
        lines.push (`\n${colorize.warning(filterWarning)}`);
    }

    let result = `${items.length} item(s) matching`;
    lines.push(`\n${colorize.success(result)}\n`);

    //Print out all the lines
    if (options.includes("-c")) {
        lines.forEach(line => console.log(line));
    } else { //Quiet, remove the colors...
        lines.forEach(line => console.log(removeColors(line)));
    }
}

function printField (field, item) {
    let fieldDepth = field.split(".");
    let value = item;
    for (let i=0; i <fieldDepth.length; i++){
        value = value[fieldDepth[i]];
    }
    let color = getColorEscString(displayTheme[field]);
    return (value ? `${color}${value}${colorReset}` : "");
}

function findRanges(line, searchFor) {
    if (searchFor.trim() === "") return [];
    const myRe = new RegExp(searchFor, "dgim");
    let matches = [];
    let foundMatch;
    while((foundMatch = myRe.exec(line)) !== null) {
        let item = { match : foundMatch[0], range : foundMatch.indices[0]};
        matches.push(item);
    }
    return matches;
}

function highlightWord (searchFor, line) {

    let reset = getColorEscString(colorCodes.Reset);
    let highlightColor = getColorEscString([colorCodes.BgYellow, colorCodes.FgBlack]);
    let searchMatches = findRanges(line, searchFor);

    searchMatches.forEach(match => {

        // Let see if the word is already in the middle of a colorized sentence.
        let surroundingColor;
        let colorMarkers = findRanges(line, "\\x1b\[[0-9;]+m");
        for (let i=0; i < colorMarkers.length; i = i +2) {
            if (match.range[0] >= colorMarkers[i].range[0] && match.range[1] <= colorMarkers[i+1].range[0]) {
                surroundingColor = colorMarkers[i];
            }
        }

        // Build the replacement
        let word = `${highlightColor}${match.match}${reset}`;
        if (surroundingColor) {
            // Stop the surrounding color and restart it after
            word = `${reset}${word}${surroundingColor.match}`;
        }

        // Finally, make the replacement
        let replacement = new RegExp(`(?<!43m)${match.match}`);
        line = line.replace(replacement, word);
    });
    return line;
}

module.exports = { displayItems };