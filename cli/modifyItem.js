const { ask, askMany, colorize, log } = require("./console");

const modifyItem = async (Controller, command) => {

    const item = {
        "deck" : "",
        "header" : "",
        "subheader" : "",
        "article" : "",
        "link" : "",
        "tags" : "",
        "comment" : ""
    }

    let ID = command.itemId;

    while (!ID) {
        ID = await ask ("ID");
        if (!ID) {
            log.info("Sorry, you need to input the ID");
        }
    }

    let dbItem = await Controller.getItem(ID);
    if (dbItem === null) {
        log.error ("Sorry, we can't find this item");
        return;
    }

    for (const prop in item) {
        item[prop] = dbItem[prop];
    }

    await askMany(item);

    log.warning(`We will be modifying the item [${ID}]: `);
    console.log (item);

    let isOkay = await ask (colorize.warning("Is that correct (y/n)"), "n");

    if (isOkay.toLowerCase() === "y") {
        let newItem = await Controller.updateItem(ID, item, "bypass");
        log.success (`The item has been modified`);
    } else {
        log.failure ("The item will *NOT* be modified");
    }
}

module.exports = { modifyItem };