const { UUIDPattern } = require("../utils");

const actionList = [ "add", "modify", "delete", "search", "tree", "filter", "help"];

class Command  {
    constructor (command = {}) {
        Object.assign(this, {
        action : "",
        itemId : "",
        options : [],
        text : ""
        }, command );
    }
};

function getArgs (argumentList) {

    let result = new Command();

    let args = argumentList.slice(2);

    // Find the action (first arg)
    result.action = "search";
    for (let x=0; x < actionList.length; x++){
        const firstLetterPattern = new RegExp(`^${actionList[x].substring(0,1)}$`);
        const fullPattern = new RegExp(`^${actionList[x]}$`);
        if (firstLetterPattern.test(args[0]) || fullPattern.test(args[0])){
            result.action = actionList[x];
            args = args.slice(1);
        }
    }

    // Now that we have an action, deal with the other args
    for (let i=0; i < args.length; i ++) {
        if (UUIDPattern.test(args[i])) {
            result.itemId = args[i];
        } else if (args[i].startsWith("--")) {
            result.options.push(args[i]);
        } else if (args[i].startsWith("-")) {
            if (args[i].indexOf("=") === -1) {
                // We are accepting "combined" options.  Example: -svc => -s -v -c
                let opts = args[i].substring(1).split("");
                for (let x=0; x < opts.length; x++) {
                    result.options.push("-" + opts[x]);
                }
            } else {
                // There was an equal sign, do not split options
                result.options.push(args[i]);
            }
        } else {
            result.text = args[i];            
        }
    }

    return result;
}

function getDepthOption (optionArr) {
    maxDepth = 3;
    optionArr.forEach(option => {
        if (/-d=[0-9]+/.test(option)) {
            maxDepth = parseInt(option.replaceAll("-d=", ""));
        }
    });
    return maxDepth;
}

module.exports = { getArgs, getDepthOption, Command };