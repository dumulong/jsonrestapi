const http = require("http");
const ControllerClass = require("./controller");
const { getFile, getReqData, searchList } = require("./utils");

const Controller = new ControllerClass();

const PORT = process.env.PORT || 5000;

const server = http.createServer(async (req, res) => {

  console.log (`requested URL(${req.method}):${req.url}`);

  const reqHash = req.headers["current-hash"];

  // /api/items : OPTION
  if (req.method === "OPTIONS") {
    res.writeHead(200, Controller.buildHeader() );
    res.end(null);
  }

  // /alive : GET (Check if app is running)
  else if ((req.url === "/api/alive") && req.method === "GET") {
    res.writeHead(200, Controller.buildHeader());
    res.end(JSON.stringify({ status : "alive and kicking"}));
  }

  // /client.(html|css|js) : GET (get client.html, client.js or client.css file)
  else if ((req.url === "/") || (req.url.match(/\/.*\.(html|css|js)/)) && req.method === "GET") {

    const mime = { "html" : "text/html", "css" : "text/css", "js" : "text/javascript" };

    const filename = ( req.url === "/" ? "client.html" : req.url.replace("/", ""));
    const ext = filename.split(".")[1];

    const fileContent = getFile(filename);
    const headers = Controller.buildHeader();
    headers["Content-Type"] = mime[ext];

    res.writeHead(200, headers);
    res.end(fileContent);
  }

  // /api/items : GET (get all items)
  else if (req.url === "/api/items" && req.method === "GET") {

    // get the items.
    const items = await Controller.getItems();

    res.writeHead(200, Controller.buildHeader());
    res.end(JSON.stringify(items));
  }

  // /api/search : POST (get items matching a search param)
  else if (req.url === "/api/search" && req.method === "POST") {

    // get the data sent along
    let payload = await getReqData(req);

    // get the items.
    let items = await Controller.getItems();

    if (payload !== "") {
      let searchParam = JSON.parse(payload);
      items = searchList (searchParam.search, items)
    }

    res.writeHead(200, Controller.buildHeader());
    res.end(JSON.stringify(items));
  }

  // /api/items/:id : GET (get a particular item)
  else if (req.url.match(/\/api\/items\/([a-zA-Z0-9]{10,})/) && req.method === "GET") {
    try {
      // get id from url
      const id = req.url.split("/")[3];

      // get item
      const item = await Controller.getItem(id);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(item));

    } catch (error) {
      res.writeHead(404, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/items/ : POST (create item)
  else if (req.url === "/api/items" && req.method === "POST") {
    try {
      // get the data sent along
      let payload = await getReqData(req);
      // create the item
      let item = await Controller.createItem(JSON.parse(payload), reqHash);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(item));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/items/:id : PATCH (update item)
  else if (req.url.match(/\/api\/items\/([a-zA-Z0-9]{10,})/) && req.method === "PATCH") {
    try {
      // get the id from the url
      const id = req.url.split("/")[3];

      // update item
      const payload = await getReqData(req);
      let updatedItem = await Controller.updateItem(id, JSON.parse(payload), reqHash);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(updatedItem));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/items/:id : DELETE (delete item)
  else if (req.url.match(/\/api\/items\/([a-zA-Z0-9]{10,})/) && req.method === "DELETE") {
    try {
      // get the id from url
      const id = req.url.split("/")[3];
      // delete item
      let deletedItem = await Controller.deleteItem(id, reqHash);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(deletedItem));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/items/:id : PATCH (update group of items)
  else if (req.url === "/api/items/group" && req.method === "PATCH") {
    try {

      // update item
      const payload = await getReqData(req);
      const updates = JSON.parse(payload);

      const updatedItems = await Controller.updateItemList(updates, reqHash);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(updatedItems));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/items/:id : DELETE (delete group of items)
  else if (req.url === "/api/items/group" && req.method === "DELETE") {
    try {

      const payload = await getReqData(req);
      const deletes = JSON.parse(payload);

      const deletedItems = await Controller.deleteItemList(deletes, reqHash);

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(deletedItems));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /api/backup : GET (backup data)
  else if (req.url.match(/\/api\/backup/) && req.method === "GET") {
    try {
      // backup data file
      const message = await Controller.backup();

      res.writeHead(200, Controller.buildHeader() );
      res.end(JSON.stringify(message));

    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // /data/data_file.js : GET
  else if (req.url.match(/\/data\/data_file.js/) && req.method === "GET") {
    try {
      // We should not send any data... this is for when the software is used
      // without the backend server (notice the ".js" instead of ".json")\
      res.writeHead(200, Controller.buildHeader() );
      res.end();
    } catch (error) {
      res.writeHead(400, Controller.buildHeader());
      res.end(JSON.stringify({ message: error.message }));
    }
  }

  // No route present
  else {
    res.writeHead(404, Controller.buildHeader());
    res.end(JSON.stringify({ message: "Route not found" }));
  }
});

server.listen(PORT, () => {
  console.log(`server started on port: ${PORT}`);
});
