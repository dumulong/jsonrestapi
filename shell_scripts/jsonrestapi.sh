#!/bin/bash
APP_PROCESS="node app.js"
APP_PORT=5001
APP_FOLDER="${HOME}/codebase/jsonrestapi"
APP_CLIENT="${APP_FOLDER}/client/client.html"  
APP_URL="http://localhost:${APP_PORT}"
IS_APP_SERVER_RUNNING="N"

PROCESS_COUNT=$(ps aux | grep -v grep | grep -ci "${APP_PROCESS}")

if [[ $PROCESS_COUNT == 1 ]]; then
    read -p "We found a process already running. Do you want to kill it (y/n)? " IS_KILLING 
    if [[ $IS_KILLING == "Y" || $IS_KILLING == "y" ]]; then
        PID=$(ps -ef | grep -v grep | grep -i "${APP_PROCESS}" | awk '{print $2}')
        echo "Killing process (${PID})"
        kill $PID
    else
        IS_APP_SERVER_RUNNING="Y"
    fi
elif [[ $PROCESS_COUNT > 1 ]]; then
    echo $(ps -ef | grep -v grep | grep -i "${APP_PROCESS}")
    echo "More than one process running, exitting!"
    exit 0
else 
    echo "No process currently running..."
fi


if [[ $IS_APP_SERVER_RUNNING == "N" ]]; then
    read -p "Do you want to start a new app server (y/n)? " START_NEW
    if [[ $START_NEW == "Y" || $START_NEW == "y" ]]; then
        echo "Starting ${APP_PROCESS}"
        CURR_DIR=$PWD
        cd $APP_FOLDER
        echo $PWD
        export PORT=$APP_PORT
        nohup $APP_PROCESS 1>./output.log 2>&1 &
        cd $CURR_DIR    
        IS_APP_SERVER_RUNNING="Y"
    fi
fi

read -p "Do you want to open it in a browser (y/n)? " OPEN_BROWSER
if [[ $OPEN_BROWSER == "Y" || $OPEN_BROWSER == "y" ]]; then
    if [[ $IS_APP_SERVER_RUNNING == "Y" ]]; then
        echo "Opening URL: ${APP_URL}"
        open -a "Google Chrome" $APP_URL
    else
        echo "Opening client: ${APP_CLIENT}"
        open -a "Google Chrome" $APP_CLIENT
    fi
fi