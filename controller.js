const {
    readJSONFile,
    writeJSONFile,
    hashString
} = require("./utils");

const ItemUtilClass = require("./ItemUtil");

const ItemUtil = new ItemUtilClass();

const DATA_FILE = "./client/data/data_file.json";
let data = readJSONFile(DATA_FILE);
let currentHash = hashString(JSON.stringify(data));

class Controller {

    // Get the hash of the current data
    buildHeader() {
        return {
            "Content-Type": "application/json",
            "current-hash": currentHash,
            "Access-Control-Allow-Origin": "*",
            "Cache-Control" : "no-cache, no-store, must-revalidate",
            "Pragma" : "no-cache",
            "Expires" : "0",
            "Access-Control-Expose-Headers": "current-hash",
            "Access-Control-Allow-Headers": "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept, Cache-Control, current-hash",
            "Access-Control-Allow-Methods": "POST, GET, PATCH, DELETE, OPTIONS"
        };
    }

    // Read and hash the actual data file to make sure that we have the
    // same file in "memory"
    isCurrent(hash) {
        if (hash == "bypass") return true;
        const fileData = readJSONFile (DATA_FILE);
        let fileHash = hashString(JSON.stringify(fileData));
        return hash === fileHash
    }

    // getting all items
    async getItems() {
        return new Promise((resolve, _) => {
            data = readJSONFile (DATA_FILE);
            currentHash = hashString(JSON.stringify(data));
            resolve(data);
        });
    }

    // getting a single item
    async getItem(id) {
        return new Promise((resolve, reject) => {
            let item = ItemUtil.findItem(id, data);
            if (item) {
                resolve(item);
            } else {
                reject({ message : `Item with id ${id} not found `});
            }
        });
    }

    // creating an item
    async createItem(item, reqHash) {
        return new Promise((resolve, reject) => {
            if (!this.isCurrent(reqHash)) {
                reject({ message : 'The current data have been updated since last retrieved. Please refresh before adding items.'});
            } else {
                let newItem = ItemUtil.addItem(item);
                data.push(newItem);
                writeJSONFile(DATA_FILE, data, true)
                currentHash = hashString(JSON.stringify(data));
                resolve(newItem);
            }
        });
    }

    // updating an item
    async updateItem(id, changes, reqHash) {
        return new Promise((resolve, reject) => {
            if (!this.isCurrent(reqHash)) {
                reject({ message: 'The current data have been updated since last retrieved. Please refresh before updating items.'});
            } else {
                let item = ItemUtil.findItem(id, data);
                if (!item) {
                    reject({ message : `No item with id ${id} found`});
                }
                ItemUtil.updateItem(changes, item);
                writeJSONFile(DATA_FILE, data, true)
                currentHash = hashString(JSON.stringify(data));
                resolve(item);
            }
        });
    }

    // deleting an item
    async deleteItem(id, reqHash) {
        return new Promise((resolve, reject) => {
            if (!this.isCurrent(reqHash)) {
                reject({message : 'The current data have been updated since last retrieved. Please refresh before deleting items.'});
            } else {
                let itemNdx = ItemUtil.findItemNdx(id, data);
                let item = data[itemNdx];
                if (itemNdx < 0) {
                    reject({ message : `No item with id ${id} found`});
                }
                data.splice(itemNdx,1);
                writeJSONFile(DATA_FILE, data, true)
                currentHash = hashString(JSON.stringify(data));
                resolve(item);
            }
        });
    }

    // updating an item
    async updateItemList(changeList, reqHash) {
        return new Promise((resolve, reject) => {
            if (!this.isCurrent(reqHash)) {
                reject({ message: 'The current data have been updated since last retrieved. Please refresh before updating items.'});
            } else {
                const updatedItems = [];
                for (let i=0; i < changeList.length; i++) {
                    const id = ItemUtil.getItemId(changeList[i]);
                    let item = ItemUtil.findItem(id, data);
                    if (!item) {
                        reject({ message : `No item with id ${id} found`});
                    }
                    const changes = {};
                    for (const property in changeList[i]) {
                        changes[property] = changeList[i][property];
                    }
                    ItemUtil.updateItem(changes, item);
                    updatedItems.push(item);
                }
                writeJSONFile(DATA_FILE, data, true)
                currentHash = hashString(JSON.stringify(data));
                resolve(updatedItems);
            }
        });
    }

    // deleting multiple items
    async deleteItemList(deleteList, reqHash) {
        return new Promise((resolve, reject) => {
            if (!this.isCurrent(reqHash)) {
                reject({message : 'The current data have been updated since last retrieved. Please refresh before deleting items.'});
            } else {

                for (let i=0; i < deleteList.length; i++) {
                    const id = ItemUtil.getItemId(deleteList[i]);
                    let itemNdx = ItemUtil.findItemNdx(id, data);
                    if (itemNdx < 0) {
                        reject({ message : `No item with id ${id} found`});
                    }
                    data.splice(itemNdx,1);
                }
                writeJSONFile(DATA_FILE, data, true)
                currentHash = hashString(JSON.stringify(data));
                resolve(deleteList);
            }
        });
    }

    // backup data file
    async backup() {
        return new Promise((resolve, reject) => {
            const ts = (new Date()).toISOString().replace(/\D/g,'');
            const backupFile = DATA_FILE.replace(/\.json/i,`_${ts}.json`);
            writeJSONFile(backupFile, data);
            resolve({ message : `Data File backed up under ${backupFile}`});
        });
    }

}
module.exports = Controller;