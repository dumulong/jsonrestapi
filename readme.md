# JSON Rest API

This is a simple rest API created in node. Because of my work environment, I've been taking great care of not using any framework or packages of any kind. I've also developed it in a way that the client is still usable without the backend API. Basically, the client folder should be self sufficient for presentation (without backend server, no add/update/delete are possible).

The idea behind it is to organize little bits of information together, like the old Rolodex. Each cards are in a Deck and has a header and subheader.

To run it, just type `node app`.  This will start the server which can provide the frontend interface and also, the backend API.

Then you can navigate to http://localhost:5000/

In order to keep it as generic as possible, I've organized the code in a certain way.  Basically, anything that need to be adapted for you own data should be found in the __ItemUtil.js__ file.

The __client.html__ file is just a example of a client consuming the API, and should be customized to your needs.

One of the main goal for the project was to ensure that we do not override the file if the file was changed in browser or browser tab.  Since we are rewriting the entire file on each update, we could lose data if we did not validate that the file that we had our browser tab was not identical to data that we have on file before Updating, adding or deleting an item.  We are using a hash of the file in order to do this verify the integrity of the file before trying to modify the file.  Each time that we get all the items, __the hash is getting reset__:

- http://localhost:5000/api/items


Then, when we call for an updating, adding or deleting item, we first verify if the hash that we have matches the hash of the file.  If it does, we make the change and update the hash with our new ash.

If the hash does not match, It mean that the file as been changed in an other browser/browser tab.  In this case, we will ask the user to first refresh the data before making such operation (i.e. call got getting all the items).

## API operations

- Retrieve all items: (Method: GET) http://localhost:5000/api/items
- Retrieve a specific item: (Method: GET) http://localhost:5000/api/items/__{GUID}__
- Add an item: (Method: POST) http://localhost:5000/api/items/__{GUID}__ (send all fields)
- Update an item: (Method: PATCH) http://localhost:5000/api/items/__{GUID}__ (Only send fields to update)
- Delete an item: (Method: DELETE) http://localhost:5000/api/items/__{GUID}__

__Special API operation__
- Backup the datafile: (Method: GET) http://localhost:5000/api/backup (./data/data_file\___{timestamp}__.json)

## Data

```json
{
  "_id": { "$oid": "5eb1895e00d6d64c72a699cb" },
  "deck": "Beer",
  "header": "Brewery",
  "subheader": "San Antonio, TX",
  "caption": "San Antonio",
  "article": "Kuenstler Brewing",
  "link": "http://kuenstlerbrewing.com",
  "tags": "",
  "comment": "",
  "owner": { "$oid": "5eacd93c5f482962aa093e32" },
  "createdAt": { "$date": "1970-01-01T00:00:00.000Z" },
  "updatedAt": { "$date": "2022-05-14T16:31:47.778Z" }
}
```

## CLI

Add the following alias for quick access
```
rcli='~/codebase/jsonrestapi/shell_scripts/apicli.sh'
```

