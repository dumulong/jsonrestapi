const ControllerClass = require("./controller");
const { getArgs } = require("./cli/args");
const { fixLength } = require("./utils");
const { addItem } = require("./cli/addItem");
const { modifyItem } = require("./cli/modifyItem");
const { removeItem } = require("./cli/removeItem");
const { searchItems } = require("./cli/search");
const { processFilter } = require("./cli/filters");
const { displayTree } = require("./cli/displayTree");

const Controller = new ControllerClass();

let command = getArgs(process.argv);

if (command.action == "add") {
    addItem (Controller, command);
} else if (command.action == "modify") {
    modifyItem (Controller, command);
} else if (command.action == "delete") {
    removeItem (Controller, command);
} else if (command.action == "search") {
    searchItems (Controller, command);
} else if (command.action == "tree") {
    displayTree (Controller, command);
} else if (command.action == "filter") {
    processFilter (Controller, command);
} else {
    let labelSize = 12;
    console.log("\nYou must enter a valid argument\n");
    console.log(`${fixLength("Add", labelSize)} :\ta[dd] [-f] [_templateItemId_] (-f : based on filter)`);
    console.log(`${fixLength("Modify", labelSize)} :\tm[odify] [_itemId_]`);
    console.log(`${fixLength("Delete", labelSize)} :\td[elete] [_itemId_]`);
    console.log(`${fixLength("Search", labelSize)} :\ts[earch] [-f] [-v] [-c] _searchFor_ (-c: color, -f: filtered, -v: verbose)`);
    console.log(`${fixLength("Tree", labelSize)} :\tt[ree] [-c] [-f] [-d=n] (-c color, -f: filtered, -d=n: n depth of the tree)`);
    console.log(`${fixLength("Filter", labelSize)} :\tf[ilter] [-s] [-c] [-d=n] [_itemId_] (-s: set, -c : clear, -d=n: n depth of the filter)`);
    console.log(`${fixLength("Help", labelSize)} :\th[elp]`);
    console.log(``);    
}