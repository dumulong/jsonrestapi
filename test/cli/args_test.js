const { getArgs, Command } = require("../../cli/args");

const commandList = [];
const resultList = [];

commandList.push ('');
resultList.push (new Command ({
    action : "search",
    itemId : "",
    options : [],
    text : ""
}));

commandList.push ('7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "search",
    itemId : "7676a878f8877ee45f733478",
    options : [],
    text : ""
}));

commandList.push ('s -c 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "search",
    itemId : "7676a878f8877ee45f733478",
    options : [ "-c" ],
    text : ""
}));

commandList.push ('s -cv 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "search",
    itemId : "7676a878f8877ee45f733478",
    options : [ "-c", "-v" ],
    text : ""
}));

commandList.push ('s -cv -d=2 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "search",
    itemId : "7676a878f8877ee45f733478",
    options : [ "-c", "-v", "-d=2" ],
    text : ""
}));

commandList.push ('a 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "add",
    itemId : "7676a878f8877ee45f733478",
    options : [],
    text : ""
}));

commandList.push ('m 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "modify",
    itemId : "7676a878f8877ee45f733478",
    options : [],
    text : ""
}));

commandList.push ('d 7676a878f8877ee45f733478');
resultList.push (new Command ({
    action : "delete",
    itemId : "7676a878f8877ee45f733478",
    options : [ ],
    text : ""
}));

commandList.push ('t -c -d=2');
resultList.push (new Command ({
    action : "tree",
    itemId : "",
    options : [ "-c", "-d=2" ],
    text : ""
}));

commandList.push ('f -d=3 "San Antonio"');
resultList.push (new Command ({
    action : "filter",
    itemId : "",
    options : [ "-d=3" ],
    text : "San Antonio"
}));

function test () {
    for (let i=0; i < commandList.length; i++ ) {
        let result = getArgs(buildArgs(commandList[i]));
        if (!compareCommand(result, resultList[i])) {
            console.log (`Command: [${commandList[i]}]`);
            console.log (`Actual : \n\t${JSON.stringify(result)}`);
            console.log (`Expected : \n\t${JSON.stringify(resultList[i])}`);
        } else {
            console.log (`Parsed correctly: ${commandList[i]}`);
        }
    }

}

function buildArgs (command) {
    let parts = [];
    if (command.trim() !== ""){
        parts = command.trim().match(/(--\S+=)?"(\\"|[^"])*"|[^ "]+/g).map(arg => arg.replace(/^"(.*)"$/, '$1'));
    }
    // Node always sends the node executable and the script file as the 2 first arguments.  We
    return ([
        'node executable ...',
        'script file ...',
        ...parts
    ])
}

function compareCommand (a, b) {
    if (a.action !== b.action) { return false; }
    if (a.itemId !== b.itemId) { return false; }
    if (a.options.length !== b.options.length) { 
        return false; 
    } else {
        for (let i=0; i < a.options.length; i++) {
            if (!b.options.includes(a.options[i])) {
                return false;
            }
        }
    }
    if (a.text !== b.text) { return false; }
    return true;
}

test();