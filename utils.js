const fs = require("fs");

const CLIENT_FOLDER = "./client/";

const UUIDPattern = new RegExp("[a-f0-9]{24}");

function fixLength(text, length, leftAlign = true) {
  let spaces = " ".repeat(length);
  if (leftAlign){
      return (text + spaces).substring(0, length);
  } else {
      return (spaces + text).slice(-length);
  }
}

function findItem(id, list) {
  return list.find((item) => item._id.$oid === id);
}

function findItemNdx(id, list) {
  return list.findIndex((item) => item._id.$oid === id);
}

function searchList (searchFor, list){

  let regExp = new RegExp(searchFor, "gi");

  if (UUIDPattern.test(searchFor)) {
    return list.filter(item => regExp.test(item._id.$oid));
  } else {
    return list.filter(item => {
      return (
        regExp.test(item.deck) ||
        regExp.test(item.header) ||
        regExp.test(item.subheader) ||
        regExp.test(item.caption) ||
        regExp.test(item.article) ||
        regExp.test(item.link) ||
        regExp.test(item.tags) ||
        regExp.test(item.comment));
    });
  
  }
}

function normalize(string) {
  return (string ? string.toLowerCase().trim() : "");
}

function sortList (item1, item2) {
  if (normalize(item1.deck) < normalize(item2.deck)) return -1;
  if (normalize(item1.deck) > normalize(item2.deck)) return 1;

  if (normalize(item1.header) < normalize(item2.header)) return -1;
  if (normalize(item1.header) > normalize(item2.header)) return 1;

  if (normalize(item1.subheader) < normalize(item2.subheader)) return -1;
  if (normalize(item1.subheader) > normalize(item2.subheader)) return 1;

  if (normalize(item1.article) < normalize(item2.article)) return -1;
  if (normalize(item1.article) > normalize(item2.article)) return 1;

  return 0;
}

function getFile (filename) {
  // We will only allow files from the client subfolder...
  return fs.readFileSync(CLIENT_FOLDER + filename);
}

function getReqData(req) {
  return new Promise((resolve, reject) => {
    try {
      let body = "";
      // listen to data sent by client
      req.on("data", (chunk) => {
        // append the string version to the body
        body += chunk.toString();
      });
      // listen till the end
      req.on("end", () => {
        // send back the data
        resolve(body);
      });
    } catch (error) {
      reject(error);
    }
  });
}

function hashString (str) {
  var hash = 0, i, chr;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr   = str.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash.toString();
}

function readJSONFile(filename) {
  let fileContent = fs.readFileSync(filename);
  return JSON.parse(fileContent);
}

function writeJSONFile (filename, data, makeJScopy = false) {
  let jsonData = JSON.stringify(data);
  fs.writeFileSync(filename, jsonData, err => {
    if (err) {
      console.error(err)
      return
    }
    //file written successfully
  });
  if (makeJScopy) {
    writeJSCopy(filename, jsonData);
  }
}

function writeJSCopy (filename, jsonData) {
  const jsFilename = filename.replace(/\.json/, '.js');
  const data = "allItems = " + jsonData
  fs.writeFileSync(jsFilename, data, err => {
    if (err) {
      console.error(err)
      return
    }
    //file written successfully
  })
}

module.exports = { fixLength, getFile, getReqData, hashString, findItem, findItemNdx, searchList, sortList, readJSONFile, writeJSONFile, UUIDPattern };
